package com.example.backend.beckend.repository;

import com.example.backend.beckend.models.Active;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActiveRepository extends JpaRepository<Active, Long> {

    List<Active> findByIdContaining(String id);

}
