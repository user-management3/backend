package com.example.backend.beckend.payload.response;

import java.util.List;

public class JwtResponse {
    private String token;
    private String type = "Bearer";
    private Long id;
    private String gambar;
    private String username;
    private String namadepan;
    private String namabelakang;
    private String alamat;
    private String deskripsi;
    private String telepon;
    private String email;
    private List<String> roles;

    public JwtResponse(String accessToken, Long id, String gambar, String username, String namadepan, String namabelakang, String alamat, String deskripsi, String telepon, String email, List<String> roles) {
        this.token = accessToken;
        this.id = id;
        this.gambar = gambar;
        this.username = username;
        this.namadepan = namadepan;
        this.namabelakang = namabelakang;
        this.alamat = alamat;
        this.deskripsi = deskripsi;
        this.telepon = telepon;
        this.email = email;
        this.roles = roles;
    }

    public String getAccessToken() {
        return token;
    }

    public void setAccessToken(String accessToken) {
        this.token = accessToken;
    }

    public String getTokenType() {
        return type;
    }

    public void setTokenType(String tokenType) {
        this.type = tokenType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNamabelakang() {
        return namabelakang;
    }

    public void setNamabelakang(String namabelakang) {
        this.namabelakang = namabelakang;
    }

    public String getNamadepan() {
        return namadepan;
    }

    public void setNamadepan(String namadepan) {
        this.namadepan = namadepan;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public List<String> getRoles() {
        return roles;
    }
}
