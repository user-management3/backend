package com.example.backend.beckend.payload.request;

import javax.persistence.Column;
import java.util.Set;

public class SignupRequest {

    @Column(name = "gambar")
    private String gambar;

    @Column(name = "username")
    private String username;

    @Column(name = "email")
    private String email;

    @Column(name = "namadepan")
    private String namadepan;

    @Column(name = "namabelakang")
    private String namabelakang;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "deskripsi")
    private String deskripsi;

    @Column(name = "telepon")
    private String telepon;

    private Set<String> role;

    @Column(name = "password")
    private String password;

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNamadepan() {
        return namadepan;
    }

    public void setNamadepan(String namadepan) {
        this.namadepan = namadepan;
    }

    public String getNamabelakang() {
        return namabelakang;
    }

    public void setNamabelakang(String namabelakang) {
        this.namabelakang = namabelakang;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getRole() {
        return this.role;
    }

    public void setRole(Set<String> role) {
        this.role = role;
    }
}
