package com.example.backend.beckend.models;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
