package com.example.backend.beckend.models;

import javax.persistence.*;

@Entity
@Table(name = "actives")
public class Active {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "keterangan")
    private String keterangan;

    @Column(name = "username")
    private String username;

    @Column(name = "waktu")
    private String waktu;

    public Active() {
    }

    public Active(String email, String keterangan, String username, String waktu, boolean b) {
        this.email = email;
        this.keterangan = keterangan;
        this.username = username;
        this.waktu = waktu;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    @Override
    public String toString() {
        return "Active{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", keterangan='" + keterangan + '\'' +
                ", username='" + username + '\'' +
                ", waktu='" + waktu + '\'' +
                '}';
    }
}
